from flask import Flask, render_template, request, redirect, flash, url_for, send_from_directory
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import FlaskForm
from werkzeug.security import generate_password_hash, check_password_hash
from werkzeug.utils import secure_filename
from wtforms import FileField, SubmitField
from wtforms.validators import InputRequired
from datetime import datetime
from slugify import slugify, slugify_unicode
from PIL import Image
import markdown, locale, os, random, logging


config = {
    "DEBUG": True,          # some Flask specific configs
    "CACHE_TYPE": "SimpleCache",  # Flask-Caching related configs
    "CACHE_DEFAULT_TIMEOUT": 300
}

app = Flask(__name__)
locale.setlocale(category=locale.LC_ALL, locale="ru_RU.UTF-8")
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///data/app.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
app.config['SECRET_KEY'] = 'my_super_secret_CODE_eveR'
app.config['UPLOAD_FOLDER'] = 'data/files'
manager = LoginManager(app)
logging.basicConfig(filename='data/logs/app.log', level=logging.DEBUG,
                    format=f'%(asctime)s %(levelname)s %(name)s %(threadName)s : %(message)s')


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(128), nullable=False, unique=True)
    password = db.Column(db.String(255), nullable=False)


class UploadFileForm(FlaskForm):
    file = FileField("File", validators=[InputRequired()])
    submit = SubmitField("Upload")


class Recipes(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=False)
    short_desc = db.Column(db.Text, nullable=False)
    description = db.Column(db.Text, nullable=False)
    type = db.Column(db.String(20), nullable=False)
    favorites = db.Column(db.String(5), nullable=False)
    date = db.Column(db.DateTime, default=datetime.utcnow)
    image = db.Column(db.String(50), nullable=False)
    image_webp = db.Column(db.String(50), nullable=False)
    url = db.Column(db.String(150), nullable=False)


class Stats(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, nullable=True)
    date = db.Column(db.DATE, default=datetime.today().date())


@manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)


@app.route('/')
@app.route('/index.html')
def index():
    recipes = Recipes.query.order_by(Recipes.id.desc()).limit(9)
    fav_recipes = Recipes.query.filter_by(favorites='on').all()
    last_three_breakfasts = Recipes.query.filter_by(type='breakfasts').order_by(Recipes.id.desc()).limit(3)
    last_three_desserts = Recipes.query.filter_by(type='desserts').order_by(Recipes.id.desc()).limit(3)
    return render_template('index.html', recipes=recipes, fav_recipes=fav_recipes,
                           last_three_breakfasts=last_three_breakfasts, last_three_desserts=last_three_desserts)


@app.route('/recipes/<int:id>/<string:url>/')
def recipe_page(id, url):
    recipes = Recipes.query.get(id)
    related_recipes = Recipes.query.filter_by(type=recipes.type).order_by(Recipes.id.desc()).limit(3)
    description = markdown.markdown(recipes.description)
    return render_template('recipe_page.html', recipes=recipes, description=description, related_recipes=related_recipes)


@app.route('/categories/<type>/')
def category_page(type):
    categories = type
    recipes = Recipes.query.filter_by(type=type).order_by(Recipes.id.desc())
    return render_template('categories.html', categories=categories, recipes=recipes)


@app.route('/cpanel', methods=['GET', 'POST'])
@login_required
def cpanel():
    all_recipe = Recipes.query.all()
    info_count = len(all_recipe)
    all_breakfasts = Recipes.query.filter_by(type='breakfasts').all()
    breakfasts_count = len(all_breakfasts)
    all_soups = Recipes.query.filter_by(type='soups').all()
    soups_count = len(all_soups)
    all_hot = Recipes.query.filter_by(type='hot').all()
    hot_count = len(all_hot)
    all_salads = Recipes.query.filter_by(type='salads').all()
    salads_count = len(all_salads)
    all_desserts = Recipes.query.filter_by(type='desserts').all()
    desserts_count = len(all_desserts)
    all_drinks = Recipes.query.filter_by(type='drinks').all()
    drinks_count = len(all_drinks)
    all_newyear = Recipes.query.filter_by(type='newyear').all()
    newyear_count = len(all_newyear)
    today = datetime.today()
    all_users = Stats.query.filter_by(date=today.date()).all()
    users_list = []
    for item in all_users:
        if item.user_id not in users_list:
            users_list.append(item.user_id)
    unique_users = len(users_list)

    return render_template('dashboard.html', info_count=info_count, breakfasts_count=breakfasts_count,
                           soups_count=soups_count, hot_count=hot_count, salads_count=salads_count,
                           desserts_count=desserts_count, drinks_count=drinks_count, unique_users=unique_users, newyear_count=newyear_count)


@app.route('/cpanel/add_recipe', methods=['GET', 'POST'])
@login_required
def add_recipe():
    form = UploadFileForm()
    if form.validate_on_submit():
        file = form.file.data
        image_type = os.path.splitext(file.filename)
        image_name = str(random.uniform(12345, 9999999))
        file.save(os.path.join(os.path.abspath(os.path.dirname(__file__)),
                               app.config['UPLOAD_FOLDER'], secure_filename(image_name + image_type[1])))
        image = Image.open(file)
        image = image.convert('RGB')
        image.save(os.path.join(os.path.abspath(os.path.dirname(__file__)),
                               app.config['UPLOAD_FOLDER'], image_name + '.webp'), 'webp')
        recipe_name = request.form.get('recipe_name')
        recipe_short_desc = request.form.get('recipe_short_desc')
        recipe_description = request.form.get('recipe_description')
        recipe_type = request.form.get('recipe_type')
        recipe_fav = request.form.get('recipe_fav')
        recipe_image = image_name + image_type[1]
        recipe_image_webp = image_name + '.webp'
        recipe_url = slugify(recipe_name).lower()
        if recipe_fav != 'on':
            recipe_fav = 'off'
        recipe = Recipes(title=recipe_name, short_desc=recipe_short_desc, description=recipe_description, type=recipe_type, favorites=recipe_fav, image=recipe_image, image_webp=recipe_image_webp, url=recipe_url)
        db.session.add(recipe)
        db.session.commit()
        return redirect(url_for('success'))
    return render_template('add_recipe.html', form=form)


@app.route('/cpanel/recipes')
@login_required
def cpanel_recipes():
    recipes = Recipes.query.order_by(Recipes.id.desc()).all()
    return render_template('recipes.html', recipes=recipes)


@app.route('/cpanel/settings')
@login_required
def settings():
    return render_template('settings.html')


@app.route('/cpanel/recipes/<int:id>/edit', methods=['GET', 'POST'])
@login_required
def recipe_edit(id):
    recipe = Recipes.query.get(id)
    if request.method == "POST":
        recipe.title = request.form.get('recipe_name')
        recipe.short_desc = request.form.get('recipe_short_desc')
        recipe.description = request.form.get('recipe_description')
        recipe.type = request.form.get('recipe_type')
        recipe.favorites = request.form.get('recipe_fav')
        recipe.url = slugify(recipe.title).lower()
        if recipe.favorites != 'on':
            recipe.favorites = 'off'
        try:
            db.session.commit()
            return redirect('/cpanel/recipes')
        except:
            return "Что-то пошло не так"
    else:
        return render_template('recipe_detail.html', recipe=recipe)


@app.route('/cpanel/recipes/<int:id>/delete')
@login_required
def recipe_delete(id):
    recipe = Recipes.query.get_or_404(id)
    try:
        db.session.delete(recipe)
        db.session.commit()
        return redirect('/cpanel/recipes')
    except:
        return "Что-то пошло не так"


@app.route('/cpanel/login', methods=['GET', 'POST'])
def login_page():
    login = request.form.get('login')
    password = request.form.get('password')

    if login and password:
        user = User.query.filter_by(login=login).first()

        if user and check_password_hash(user.password, password):
            login_user(user)

            next_page = request.args.get('next')

            return redirect(next_page)
        else:
            flash('Логин или пароль неверны')
    else:
        flash('Пожалуйста, заполните поля логина и пароля')

    return render_template('login.html')


# @app.route('/cpanel/register-new-user', methods=['GET', 'POST'])
# @login_required
# def register():
#     login = request.form.get('login')
#     password = request.form.get('password')
#     password2 = request.form.get('password2')
#
#     if request.method == 'POST':
#         if not (login or password or password2):
#             flash('Please, fill all fields!')
#         elif password != password2:
#             flash('Passwords are not equal!')
#         else:
#             hash_pwd = generate_password_hash(password)
#             new_user = User(login=login, password=hash_pwd)
#             db.session.add(new_user)
#             db.session.commit()
#
#             return redirect(url_for('login_page'))
#
#     return render_template('register.html')


@app.route('/cpanel/logout', methods=['GET', 'POST'])
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.after_request
def redirect_to_signin(response):
    if response.status_code == 401:
        return redirect(url_for('login_page') + '?next=' + request.url)

    return response


@app.route('/cpanel/success')
@login_required
def success():
    return render_template('success.html')


@app.route('/data/files/<path:path>')
def send_image(path):
    return send_from_directory('data/files', path)


@app.route('/robots.txt')
def robots_txt():
    return send_from_directory(app.static_folder, request.path[1:])


@app.route('/sitemap.xml')
def sitemap_xml():
    return send_from_directory(app.static_folder, request.path[1:])


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=5005)

