import telebot
import random
from telebot import types
from app import Recipes, Stats, db
from datetime import datetime
import time
import logging

# PROD
bot = telebot.TeleBot('5775607560:AAG3pRmePXHK4wuZybaRrmbXSLE04NT3aF8')
# DEV
# bot = telebot.TeleBot('5597091100:AAEMDOfQgjTna9l6sXSVOTNTk_xfVBXAmdo')

logger = telebot.logger
telebot.logger.setLevel(logging.DEBUG)


@bot.message_handler(commands=['start'])
def start(message):
    get_user_id = message.from_user.id
    stats = Stats(user_id=get_user_id)
    db.session.add(stats)
    db.session.commit()
    print("STAT_START " + str(get_user_id))
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    btn1 = types.KeyboardButton("🍳 Завтраки")
    btn2 = types.KeyboardButton("🥘 Супы")
    btn3 = types.KeyboardButton("🍝 Горячее")
    btn4 = types.KeyboardButton("🥗 Салаты")
    btn5 = types.KeyboardButton("🍰 Десерты")
    btn6 = types.KeyboardButton("🥤 Напитки")
    # btn6 = types.KeyboardButton("🎄 Новый год")

    markup.add(btn1, btn2, btn3, btn4, btn5, btn6)
    bot.send_message(message.chat.id,
                     text="Поздравляю, {0.first_name}!\n\n"
                          "Теперь у тебя есть личная книга рецептов, которые не оставят равнодушным даже самого большого привереду.  \n\n"
                          "Все просто: ты нажимаешь нужную кнопку, я рандомно выдаю прекрасный рецепт с полным описанием. \n\n"
                          "Тебе понравится. Обещаю ☺️".format(
                         message.from_user), reply_markup=markup)


@bot.message_handler(content_types=['text', 'photo'])
def func(message):
    get_user_id = message.from_user.id
    stats = Stats(user_id=get_user_id)
    db.session.add(stats)
    db.session.commit()
    print("STAT " + str(get_user_id))
    if (message.text == "🍳 Завтраки"):
        random_recipes_b = Recipes.query.filter_by(type='breakfasts').all()
        recipe_count_b = len(random_recipes_b)
        random_b_id = int(random.uniform(0, recipe_count_b))
        mess = f'<b>{random_recipes_b[random_b_id].title}</b>\n\n{random_recipes_b[random_b_id].description}'
        bot.send_photo(message.chat.id, photo=open('/app/data/files/' + random_recipes_b[random_b_id].image, 'rb'))
        bot.send_message(message.chat.id, mess, parse_mode='html')

    elif (message.text == "🥘 Супы"):
        random_recipes_s = Recipes.query.filter_by(type='soups').all()
        recipe_count_s = len(random_recipes_s)
        random_s_id = int(random.uniform(0, recipe_count_s))
        mess = f'<b>{random_recipes_s[random_s_id].title}</b>\n\n{random_recipes_s[random_s_id].description}'
        bot.send_photo(message.chat.id, photo=open('/app/data/files/' + random_recipes_s[random_s_id].image, 'rb'))
        bot.send_message(message.chat.id, mess, parse_mode='html')

    elif (message.text == "🍝 Горячее"):
        random_recipes_h = Recipes.query.filter_by(type='hot').all()
        recipe_count_h = len(random_recipes_h)
        random_h_id = int(random.uniform(0, recipe_count_h))
        mess = f'<b>{random_recipes_h[random_h_id].title}</b>\n\n{random_recipes_h[random_h_id].description}'
        bot.send_photo(message.chat.id, photo=open('/app/data/files/' + random_recipes_h[random_h_id].image, 'rb'))
        bot.send_message(message.chat.id, mess, parse_mode='html')

    elif (message.text == "🥗 Салаты"):
        random_recipes_sl = Recipes.query.filter_by(type='salads').all()
        recipe_count_sl = len(random_recipes_sl)
        random_sl_id = int(random.uniform(0, recipe_count_sl))
        mess = f'<b>{random_recipes_sl[random_sl_id].title}</b>\n\n{random_recipes_sl[random_sl_id].description}'
        bot.send_photo(message.chat.id, photo=open('/app/data/files/' + random_recipes_sl[random_sl_id].image, 'rb'))
        bot.send_message(message.chat.id, mess, parse_mode='html')

    elif (message.text == "🍰 Десерты"):
        random_recipes_d = Recipes.query.filter_by(type='desserts').all()
        recipe_count_d = len(random_recipes_d)
        random_d_id = int(random.uniform(0, recipe_count_d))
        mess = f'<b>{random_recipes_d[random_d_id].title}</b>\n\n{random_recipes_d[random_d_id].description}'
        bot.send_photo(message.chat.id, photo=open('/app/data/files/' + random_recipes_d[random_d_id].image, 'rb'))
        bot.send_message(message.chat.id, mess, parse_mode='html')

    elif (message.text == "🥤 Напитки"):
        random_recipes_dr = Recipes.query.filter_by(type='drinks').all()
        recipe_count_dr = len(random_recipes_dr)
        random_dr_id = int(random.uniform(0, recipe_count_dr))
        mess = f'<b>{random_recipes_dr[random_dr_id].title}</b>\n\n{random_recipes_dr[random_dr_id].description}'
        bot.send_photo(message.chat.id, photo=open('/app/data/files/' + random_recipes_dr[random_dr_id].image, 'rb'))
        bot.send_message(message.chat.id, mess, parse_mode='html')


    # elif (message.text == "🎄 Новый год"):
    #     random_recipes_dr = Recipes.query.filter_by(type='newyear').all()
    #     recipe_count_dr = len(random_recipes_dr)
    #     random_dr_id = int(random.uniform(0, recipe_count_dr))
    #     mess = f'<b>{random_recipes_dr[random_dr_id].title}</b>\n\n{random_recipes_dr[random_dr_id].description}'
    #     bot.send_photo(message.chat.id, photo=open('/app/data/files/' + random_recipes_dr[random_dr_id].image, 'rb'))
    #     bot.send_message(message.chat.id, mess, parse_mode='html')
    #
    # elif (message.text == "🥦 У меня есть!"):
    #     mess = bot.send_message(message.chat.id, 'Введите название продукта')
    #     bot.register_next_step_handler(mess, ihave)

    elif (message.text == "show_stat"):
        today = datetime.now().strftime("%Y-%m-%d")
        get_users = Stats.query.filter_by(date=today).all()
        users_list = []
        for item in get_users:
            if item.user_id not in users_list:
                users_list.append(item.user_id)
        mess = f'Уникальных пользователей сегодня: {len(users_list)}'
        bot.send_message(message.chat.id, mess, parse_mode='html')
    else:
        bot.send_message(message.chat.id, text="Выберите тип рецепта")


# def ihave(message):
#     get_user_id = message.from_user.id
#     stats = Stats(user_id=get_user_id)
#     db.session.add(stats)
#     db.session.commit()
#     f_ingredient = message.text.lower()
#     random_recipes_i = Recipes.query.filter_by(ingredient=f_ingredient).all()
#     recipe_count_i = len(random_recipes_i)
#     random_i_id = int(random.uniform(0, recipe_count_i))
#
#     if recipe_count_i != 0:
#         mess = f'{random_recipes_i[random_i_id].name}\n\n{random_recipes_i[random_i_id].description}'
#         bot.send_photo(message.chat.id,
#                        photo=open('/app/static/files/' + random_recipes_i[random_i_id].image_name, 'rb'))
#         bot.send_message(message.chat.id, mess, parse_mode='html')
#     else:
#         bot.send_message(message.chat.id, text="Нет рецепта с такими продуктами")


# bot.polling(none_stop=True)

if __name__ == '__main__':
    while True:
        try:
            bot.polling(non_stop=True)
        except Exception as e:
            time.sleep(3)
            print(e)
